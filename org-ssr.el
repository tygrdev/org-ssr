;;; org-ssr.el --- Server side rendering in the current directory -*- lexical-binding: t -*-

;; Copyright (C) 2021-2023 Free Software Foundation, Inc.

;; Author: Amy Grinn <grinn.amy@gmail.com>
;; Version: 2.1.1
;; File: org-ssr.el
;; Package-Requires: ((emacs "27.2") (web-server "0.1"))
;; Keywords: tools
;; URL: https://gitlab.com/grinn.amy/org-ssr

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; The command `org-ssr' will start a server-side renderer in the
;; current directory.  If called with a prefix argument, files will be
;; served recursively.  The ip address and port will be displayed in
;; the minibuffer.  Navigating to this address in the browser will
;; list all available export formats for all available org files (as
;; well as all subdirectories if served recursively).

;; A cache of exported files is saved in the hash table
;; `org-ssr--cache'.  To clear this cache, use the command
;; `org-ssr-clear-cache'.

;; Use the built-in command `list-processes' to manage instances of
;; `org-ssr' servers and view their listening ports.

;; This package is an extension of an example created Eric Schulte for
;; web-server.el available here:

;; https://github.com/eschulte/emacs-web-server/blob/master/examples/007-org-mode-file-server.el

;;; Code:

;;;; Requirements

(require 'ox)
(require 'ox-org)
(require 'ox-html)
(require 'ox-latex)
(require 'ox-ascii)
(require 'web-server)

;;;; Options

(defgroup org-ssr nil
  "Customization options for org-ssr."
  :group 'applications)

(defcustom org-ssr-recursive nil
  "Automatically serve all files recursively.

This makes `org-ssr' behave like `C-u org-ssr'."
  :type 'boolean)

(defcustom org-ssr-babel nil
  "Automatically execute babel src blocks when exporting (unsafe).

Any #+CALL: properties will be evaluated without confirmation."
  :type 'boolean)

(defcustom org-ssr-no-cache nil
  "Disable caching; run export function for every request."
  :type 'boolean)

;;;; Variables

(defvar org-ssr--query nil
  "Query parameters from a request.")

(defvar org-ssr--cache (make-hash-table :test #'equal))

;;;; Structs

(cl-defstruct org-ssr-export
  "Struct for each element of the org-ssr--cache."
  mtime sentinel-or-result)

;;;; Commands

;;;###autoload
(defun org-ssr (&optional arg)
  "Start a server side renderer for all org files in the current directory.

When called interactively with a `\\[univeral-argument]' prefix
argument ARG, serve all files recursively.

With a `\\[universal-argument] \\[universal-argument]' prefix
argument, the web server will listen on all
interfaces (connections can be made on the local network)

with a `\\[universal-argument] \\[universal-argument]
\\[universal-argument]' prefix argument, the web server will
server files recursively and listen on all interfaces."
  (interactive "P")
  (let ((recursive (or (equal arg '(4)) (equal arg '(64))))
        (host (if (or (equal arg '(16)) (equal arg '(64)))
                  "0.0.0.0"
                nil)))
    (org-ssr--start :recursive recursive :host host)))

;;;###autoload
(defun org-ssr-clear-cache ()
  "Clear the cache of exported files for org-ssr."
  (interactive)
  (setq org-ssr--cache (make-hash-table :test #'equal)))

;;;###autoload
(defun org-ssr-query (param &optional default type)
  "Get the query param PARAM or return DEFAULT.

Convert to TYPE (either `number' or `string') if specified."
  (if (boundp 'org-ssr--query)
      (if-let ((val (plist-get org-ssr--query param)))
          (if (eq 'number type)
              (string-to-number val)
            val)
        default)
    default))

;;;;; Server

(cl-defun org-ssr--start (&key recursive host port dir babel verbose no-cache)
  "Start a server-side renderer for all org files in the current directory.

If RECURSIVE is non-nil, include all files recursively.

HOST is the address to listen on.  Set to '0.0.0.0' to listen on
all interfaces.

PORT will be used to serve the files.  If not provided, one will
be generated.

If DIR is provided, serve that directory.  Otherwise, serve
`default-directory'.

If BABEL is non-nil, any #+CALL property will be executed without
prompts.

VERBOSE can either be t to log all messages from the server or
`2' to log all messages from the server and the export processes.

If NO-CACHE is set to t, always run the export function for every
request."
  (prog1
      (let* ((org-ssr-recursive (or org-ssr-recursive (not (not recursive))))
             (org-ssr-babel (or org-ssr-babel babel))
             (org-ssr-no-cache (or org-ssr-no-cache no-cache))
             (server (ws-start
                      (org-ssr--handler (expand-file-name (or dir default-directory)))
                      (or port t)
                      nil
                      :org-ssr t
                      :host host))
             (port (with-slots (process) server
                     (process-contact process :service)))
             (addresses (org-ssr--get-addresses)))
        (message (concat
                  (format "Org ssr is serving folder %s" (or dir default-directory))
                  (if org-ssr-recursive " recursively")
                  "\n\nAvailable at:\n"
                  (mapconcat
                   (lambda (address)
                     (format "\thttp://%s:%s" address port))
                   addresses
                   "\n"))))
    (if verbose
        (progn
          ;; Add stderr filter to all make-process calls
          (advice-add 'make-process :filter-args
                      #'org-ssr--make-process-filter-args)
          
          ;; Add log function to all make-network-process calls
          (advice-add 'make-network-process :filter-args
                      #'org-ssr--make-network-process-filter-args)
          
          (if (eq 2 verbose)
              ;; Add filter function to all start-process calls
              (advice-add 'start-process :filter-return
                          #'org-ssr--start-process-filter-return)
            (advice-remove 'start-process #'org-ssr--start-process-filter-return)))
      (progn
        (advice-remove 'make-process #'org-ssr--make-process-filter-args)
        (advice-remove 'make-network-process #'org-ssr--make-network-process-filter-args)
        (advice-remove 'start-process #'org-ssr--start-process-filter-return)))))

;;;; Web server handler

(defun org-ssr--handler (docroot)
  "Return a ws handler which serves files from DOCROOT.

If RECURSIVE is non-nil, serve all files in DOCROOT recursively.

If BABEL is non-nil, execute any #+CALL: properties without
confirmation.

If NO-CACHE is non-nil, export the org file on every request and
don't save exported results to `org-ssr--cache'."
  `(lambda (request)
     (with-slots (process headers) request
       (let* ((query (mapcan
                      (lambda (el)
                        (list (intern (concat ":" (car el))) (cdr el)))
                      (seq-filter
                       (lambda (el) (stringp (car el)))
                       headers)))
              (req (substring (cdr (assoc :GET headers)) 1))
              (raw-path (ws-in-directory-p ,docroot req))
              (path (cond
                     ((and raw-path
                           (file-directory-p raw-path)
                           (file-exists-p (expand-file-name "index.org" raw-path)))
                      ;; If index.org exists in requested directory, serve it
                      ;; as html instead of listing directory contents.
                      (expand-file-name "index.html" raw-path))
                     ((and raw-path (file-exists-p (concat raw-path ".org")))
                      ;; .html can be excluded from request
                      (concat raw-path ".html"))
                     (t
                      raw-path)))
              (src (and path (concat (file-name-sans-extension path) ".org")))
              (ext (and path (downcase (or (file-name-extension path) ""))))
              (type (and ext (cond
                              ((string= ext "html") 'html)
                              ((string= ext "tex") 'latex)
                              ((string= ext "txt") 'ascii)
                              ((string= ext "pdf") 'pdf)
                              ((and (string= ext "xml") (featurep 'ox-rss))
                               'rss)
                              ((string= ext "org") 'org)))))
         (cond
          ((or (not path)
               (and (not (file-exists-p path))
                    (or (not (file-exists-p src)) (not type)))
               (and (not ,org-ssr-recursive)
                    (not (string= ,docroot (file-name-directory path)))))
           (if-let* ((error-page-dir (locate-dominating-file path "404.org"))
                     (error-page (expand-file-name
                                  (concat "404." (if type ext "html"))
                                  error-page-dir))
                     (error-page-src (expand-file-name "404.org" error-page-dir)))
               (if (if ,org-ssr-recursive
                       (string-prefix-p ,docroot error-page)
                     (string= ,docroot (file-name-directory error-page)))
                   (org-ssr--render :process process
                                    :key error-page
                                    :org-file error-page-src
                                    :type (or type 'html)
                                    :babel ,org-ssr-babel
                                    :status-code 404
                                    :no-cache ,org-ssr-no-cache
                                    :query query)
                 (ws-send-404 process (format "%s does not exist" req)))
             (ws-send-404 process (format "%s does not exist" req))))
          ((file-directory-p path) ;; List directory contents
           (org-ssr--send process "text/html"
                          (org-ssr--create-index path ,org-ssr-recursive)))
          ((file-exists-p src) ;; Export org file as requested
           (org-ssr--render :process process
                            :key path
                            :org-file src
                            :type type
                            :babel ,org-ssr-babel
                            :no-cache ,org-ssr-no-cache
                            :query query))
          ((file-exists-p path) ;; Serve static assets
           (if (string-match-p "\\.mjs\\'" path)
               (ws-send-file process path "application/javascript")
             (ws-send-file process path)))
          (t
           (ws-send-500)))))))


;;;; Renderer

(cl-defun org-ssr--render (&key process key org-file type babel status-code no-cache query)
  "Asynchronously render ORG-FILE as TYPE.

Afterwards, send it to PROCESS and save it in `org-ssr--cache'
under KEY.

If BABEL is non-nil, execute any #+CALL: properties without
confirmation.

STATUS-CODE should be a number or nil for 200.

If NO-CACHE is non-nil, don't save or load exported results from
`org-ssr--cache', export on every request.

QUERY is a plist of query parameters to be used as org babel
variables."
  (let* ((content-type (if (eq type 'html)
                           "text/html"
                         "text/plain"))
         (mtime (file-attribute-modification-time
                 (file-attributes org-file))))
    (if-let* ((export (gethash key org-ssr--cache))
              (same-file (org-ssr--timestamp= mtime (org-ssr-export-mtime export)))
              (sentinel-or-result (org-ssr-export-sentinel-or-result export)))
        (condition-case nil
            (cond
             ((functionp sentinel-or-result)
              (prog1 :keep-alive
                (add-function :after sentinel-or-result
                              `(lambda (p _)
                                 (when (eq (process-status p) 'exit)
                                   (let ((output (org-ssr-export-sentinel-or-result
                                                  (gethash ,key org-ssr--cache))))
                                     (if (not (stringp output))
                                         ;; Invalid output
                                         (progn
                                           (remhash ,key org-ssr--cache)
                                           (if (process-live-p ,process)
                                               (ws-send-500 ,process)))
                                       (if (eq 'pdf type)
                                           (if (file-exists-p output)
                                               (if (process-live-p ,process)
                                                   (ws-send-file ,process output))
                                             ;; Temp file was deleted, recreate it
                                             (remhash ,key org-ssr--cache)
                                             (org-ssr--render :process ,process
                                                              :key ,key
                                                              :org-file ,org-file
                                                              :type ,type
                                                              :babel ,babel
                                                              :status-code ,status-code
                                                              :no-cache ,no-cache))
                                         (if (process-live-p ,process)
                                             (org-ssr--send ,process ,content-type output ,status-code)))))
                                   (delete-process ,process))))))
             ((stringp sentinel-or-result)
              (if (eq 'pdf type)
                  (if (file-exists-p sentinel-or-result)
                      (ws-send-file process sentinel-or-result)
                    ;; Temp file was deleted, recreate it
                    (remhash key org-ssr--cache)
                    (org-ssr--render :process process
                                     :key key
                                     :org-file org-file
                                     :type type
                                     :babel babel
                                     :status-code status-code
                                     :no-cache no-cache))
                (org-ssr--send process content-type sentinel-or-result status-code))))
          (error (remhash key org-ssr--cache)
                 (ws-send-500 process)))
      (prog1 :keep-alive
        (message "Creating %s export of %s" (symbol-name type) org-file)
        (with-temp-buffer
          (insert-file-contents org-file)
          (hack-local-variables)
          (let* ((default-directory (file-name-directory org-file))
                 (properties (org-collect-keywords '("BABEL" "NO_CACHE")))
                 (no-cache (or no-cache (car (alist-get "NO_CACHE" properties nil nil #'equal))))
                 (babel (or babel (car (alist-get "BABEL" properties nil nil #'equal))))
                 (sentinel (org-export-async-start
                               `(lambda (output)
                                  (message "Done (%s)" ,key)
                                  (if (eq 'pdf ',type)
                                      (progn
                                        (let ((tex-file (make-temp-file nil)))
                                          (with-temp-file tex-file
                                            (insert output))
                                          (let ((output-file (org-latex-compile tex-file)))
                                            (if (not ,no-cache)
                                                (puthash ,key (make-org-ssr-export
                                                               :mtime ',mtime
                                                               :sentinel-or-result output-file)
                                                         org-ssr--cache))
                                            (if (process-live-p ,process)
                                                (ws-send-file ,process output-file)))))
                                    (if (not ,no-cache)
                                        (puthash ,key (make-org-ssr-export
                                                       :mtime ',mtime
                                                       :sentinel-or-result output)
                                                 org-ssr--cache))
                                    (if (process-live-p ,process)
                                        (org-ssr--send ,process ,content-type output ,status-code))
                                    (delete-process ,process)))
                             `(let ((org-babel-confirm-evaluate-answer-no ,(not babel))
                                    (org-confirm-babel-evaluate ,(not babel)))
                                (setq org-ssr--query (quote ,query))
                                (org-export-as
                                 ',(if (eq 'pdf type) 'latex type)
                                 nil nil nil (list :input-file ,org-file))))))
            (if (not no-cache)
                (puthash key (make-org-ssr-export
                              :mtime mtime
                              :sentinel-or-result sentinel)
                         org-ssr--cache))))))))

(defun org-ssr--create-index (dir recursive)
  "Get directory listing for DIR as html string.

If RECURSIVE is non-nil include subdirectories."
  (concat "<ul>"
          (mapconcat
           (lambda (f)
             (let* ((full (expand-file-name f dir))
                    (end (if (file-directory-p full) "/" ""))
                    (url (url-encode-url (concat f end))))
               (format "<li><a href=%s>%s</li>" url f)))
           (apply #'append
                  (and recursive
                       (seq-filter
                        #'file-directory-p
                        (directory-files dir)))
                  (mapcar
                   (lambda (f)
                     (list (concat f ".txt")
                           (concat f ".tex")
                           (concat f ".html")
                           (concat f ".org")))
                   (mapcar
                    #'file-name-sans-extension
                    (directory-files dir nil "\\.org\\'"))))
           "\n")
          "</ul>"))

(defun org-ssr--send (process content-type content &optional status-code)
  "Send CONTENT of type CONTENT-TYPE to PROCESS.

PROCESS should be a web-server handler process.

STATUS-CODE should be a number or nil for 200."
  (ws-response-header process (or status-code 200) (cons "Content-Type" content-type))
  (process-send-string process content))

;;;; Logging

(defun org-ssr--print-debug (proc message)
  "Pretty-print MESSAGE from PROC."
  (dolist (str (split-string message "\n"))
    (if (not (string-match-p "^[[:blank:]]*$" str))
        (message "%s %s %s"
                 (format-time-string "%FT%T%z")
                 (process-name proc)
                 str))))

(defun org-ssr--make-process-filter-args (args)
  "Add logging filter function to `make-process' ARGS."
  (if (string= "org-export-process" (plist-get args :name))
      (plist-put args :stderr
                 (make-pipe-process
                  :name (plist-get args :name)
                  :filter #'org-ssr--print-debug)))
  args)

(defun org-ssr--make-network-process-filter-args (args)
  "Add logging filter function to `make-network-process' ARGS."
  (if (plist-get args :org-ssr)
      (plist-put args :log
                 (lambda (proc client message)
                   (org-ssr--print-debug proc message)
                   (add-function :after (process-filter client)
                                 #'org-ssr--print-debug))))
  args)

(defun org-ssr--start-process-filter-return (proc)
  "Add logging function after `start-process' filter function.

PROC is the process the filter function is associated with"
  (if (string-match-p "^org-export-process" (process-name proc))
      (add-function :after (process-filter proc)
                    #'org-ssr--print-debug))
  proc)

;;;; Utility expressions

(defun org-ssr--timestamp= (a b)
  "Return t if A and B elisp timestamps are equal, nil otherwise."
  (not (catch 'conflict
         (if (not (= (length a) (length b)))
             (throw 'conflict t))
         (let ((i 0))
           (while (< i (length a))
             (if (not (= (nth i a) (nth i b)))
                 (throw 'conflict t))
             (cl-incf i))))))

(defun org-ssr--get-addresses ()
  "Get all addresses the org-ssr instance is available at."
  (append '("localhost")
          (mapcar
           (lambda (info)
             (format-network-address (car info) t))
           (seq-filter
            (lambda (info)
              (= 3 (length
                    (seq-intersection
                     '(broadcast up running)
                     (car (last info))))))
            (mapcar
             (lambda (interface) (network-interface-info (car interface)))
             (network-interface-list nil 'ipv4))))))

(provide 'org-ssr)

;;; org-ssr.el ends here
